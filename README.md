# dotfiles

My dotfiles managed by [chezmoi](https://www.chezmoi.io/).

## Deploy

1. Install prerequisites:
    * `bsdtar`: [Because most `unzip` implementations do not allow piping in input.](https://stackoverflow.com/a/52759718)
    * [`pre-commit`](https://pre-commit.com/): `pip install --user pre-commit`

2. Install optional software:
    * `sway`
    * `cool-retro-term`

3. [Install `chezmoi`](https://www.chezmoi.io/install/)

4. Deploy
    1. `chezmoi init --apply --verbose git@gitlab.com:jdmarble/dotfiles.git`
    2. `chezmoi cd`
    3. `pre-commit install`
